# multimeter

Tension/current/power monitoring, based on arduino and INA2XX to measure these informations for other arduino/raspberry projects.

TODOs:
- manage cursors on graph
- add/remove graphs

Ideas for future features :
- add a manual offset on axes
- light restitution (only 1 point every second for example) with min/max/average measured between two abscissa points (keep the same acquisition speed on arduino and communication format) => display data in a statistical style (box plot).
- add a menu to change color style of the window
- check Qt soft/arduino version at startup.
- measure for more than one day and QDateTime axis.



Documentations/technical points:


- cursor placement/mouse following:
https://www.advsofteng.com/doc/cdcppdoc/trackaxisqt.htm
- time axis update:
http://www.programmersought.com/article/641170361/

- fritzing adafruit lib :
https://github.com/adafruit/Fritzing-Library
https://learn.adafruit.com/using-the-adafruit-library-with-fritzing/import-the-library-into-fritzing




Architecture points for future devs :
- make a file class to manage load/save/export data to file.


dependencies:
- Qt 5.14
- qcustomplot (version 2.0.1)
- arduino environement
- adafruit lib for INA219 and dependencies (arduino and fritzing).