import time
import digitalio
import board
from typing import List
from PIL import Image, ImageDraw, ImageFont
from adafruit_rgb_display import ili9341
from threading import Thread

# original source code:
# https://learn.adafruit.com/adafruit-2-8-and-3-2-color-tft-touchscreen-breakout-v2/python-wiring-and-setup

# Config for display baudrate (default max is 24mhz):
BAUDRATE = 60000000


class DataPoint:
    def __init__(self, label="", value=None, color="#FFFFFF") -> None:
        self.label: str = label
        self.value = value
        self.color = color

    def disp_data(self):
        return f"{self.label}: {self.value}"


class ProfilerDisplay(Thread):
    def __init__(self) -> None:
        Thread.__init__(self)
        # Configuration for CS and DC pins (these are PiTFT defaults):
        cs_pin = digitalio.DigitalInOut(board.CE0)
        dc_pin = digitalio.DigitalInOut(board.D25)
        reset_pin = digitalio.DigitalInOut(board.D24)
        # Setup SPI bus using hardware SPI:
        self.spi = board.SPI()
        self.disp = ili9341.ILI9341(
            self.spi,
            rotation=90,  # 2.2", 2.4", 2.8", 3.2" ILI9341
            cs=cs_pin,
            dc=dc_pin,
            rst=reset_pin,
            baudrate=BAUDRATE,
        )
        self.data_points: List[DataPoint] = []

        # Create blank image for drawing.
        # Make sure to create image with mode 'RGB' for full color.
        self.height = self.disp.width  # we swap height/width to rotate it to landscape!
        self.width = self.disp.height

        self.image = Image.new("RGB", (self.width, self.height))
        # Get drawing object to draw on image.
        self.draw = ImageDraw.Draw(self.image)

        # Draw a black filled box to clear the image.
        self.draw.rectangle((0, 0, self.width, self.height), outline=0, fill=(0, 0, 0))
        self.disp.image(self.image)
        self.start()

    def run(self):
        # Load a TTF font.  Make sure the .ttf font file is in the
        # same directory as the python script!
        # Some other nice fonts to try: http://www.dafont.com/bitmap.php
        self.font = ImageFont.truetype(
            "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 24
        )
        while True:
            self.update_display_content()
            # Display image.
            T1 = time.time()
            self.disp.image(self.image)
            print(f"time to draw image : {time.time() - T1}")
            # time.sleep(0.1)

    def update_display_content(self):
        # Draw a black filled box to clear the image.
        self.draw.rectangle((0, 0, self.width, self.height), outline=0, fill=0)
        # Write four lines of text.
        x = 0
        y = -2
        for data_point in self.data_points:
            display_text = data_point.disp_data()
            self.draw.text((x, y), display_text, font=self.font, fill=data_point.color)
            y += self.font.getbbox(display_text)[3]
        # self.draw.text((x, y), CPU, font=self.font, fill="#FFFF00")
        # y += self.font.getbbox(CPU)[3]
        # self.draw.text((x, y), MemUsage, font=self.font, fill="#00FF00")
        # y += self.font.getbbox(MemUsage)[3]
        # self.draw.text((x, y), Disk, font=self.font, fill="#0000FF")
        # y += self.font.getbbox(Disk)[3]
        # self.draw.text((x, y), Temp, font=self.font, fill="#FF00FF")
        # y += self.font.getbbox(Temp)[3]
        # self.draw.text((x, y), f"time : {last_time}", font=self.font, fill="#FF0000")

    def update_data_point(self, label, value, color="#FFFFFF"):
        for data_point in self.data_points:
            if data_point.label == label:
                data_point.value = value
                data_point.color = color
                return

        self.data_points.append(DataPoint(label, value, color))

    def update_data_points(self, points: List[DataPoint]):
        self.data_points = points
