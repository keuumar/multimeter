import time
import subprocess
from display_manager import ProfilerDisplay, DataPoint

CMD = [
    {"label":"IP","cmd":"hostname -I | cut -d' ' -f1", "color": "#FFFFFF"},
    {"label":"CPU", "cmd": "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'", "color": "#FF0000"},
    {"label":"MemUsage", "cmd": "free -m | awk 'NR==2{printf \"Mem: %s/%s MB  %.2f%%\", $3,$2,$3*100/$2 }'", "color": "#00FF00"},
    {"label":"Disk", "cmd": 'df -h | awk \'$NF=="/"{printf "Disk: %d/%d GB  %s", $3,$2,$5}\'', "color": "#0000FF"},
    {"label":"Temp", "cmd": "cat /sys/class/thermal/thermal_zone0/temp |  awk '{printf \"CPU Temp: %.1f C\", $(NF-0) / 1000}'", "color": "#FF00FF"},
    {"label":"time", "fct": time.time, "color": "#00FFFF"},

]

def refresh_value(item:dict):
    if "cmd" in item.keys():
        return subprocess.check_output(item["cmd"], shell=True).decode("utf-8")
    elif "fct" in item.keys():
        return item["fct"]()
    elif "fixed_value" in item.keys():
        return item["fixed_value"]
    else:
        print(f"NO ORDER TO REFRESH")
        return None

def main():
    data_points = []
    disp = ProfilerDisplay()
    num_loop = 0
    while True:
        for item in CMD:
            found_point = False
            for data_point in data_points:
                if data_point.label == item["label"]:
                    data_point.value = refresh_value(item)
                    data_point.color = item["color"]
                    found_point = True
                    continue
            
            if found_point:
                continue

            data_points.append(DataPoint(item["label"], refresh_value(item), item["color"]))

        disp.update_data_points(data_points)

        time.sleep(0.1)
        num_loop += 1
        if num_loop % 10 == 0:
            print("main still alive")


if __name__ == "__main__":
    main()
