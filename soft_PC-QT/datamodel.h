#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QWidget>
#include <QTime>

#define INIT_MIN_VALUES             1000000000.0
#define INIT_MAX_VALUES             0.0
#define MSEC_TO_UPDATE_GRAPH_DATA   50

#define SEPARATOR   ';'
#define NEW_LINE    '\n'

#define LAST_DATA   -1

typedef struct dataPoint
{
    double time;
    QTime qtime;
    double tension;
    double intensity;
    double power;
} dataPoint;

typedef struct dataSeries
{
    QVector<double> timeSerie;
    QVector<double> tensionSerie;
    QVector<double> intensitySerie;
    QVector<double> powerSerie;
} dataSeries;

enum graphIndex { tension = 0, intensity, power, sizeEnumGraphIndex };

enum fileOperation { operationNone = 0, operationLoad, operationSave, sizeEnumFileOperation };

class dataModel : public QWidget
{
    Q_OBJECT
public:
    static dataModel* Instance();
    dataSeries * getAllData();
    dataPoint getData(int index = LAST_DATA);
    dataPoint findData(double time);
    QVector<double> getMinValues();
    QVector<double> getMaxValues();

public slots:
    void processRawData(QByteArray rawData);
    void clearData();

    void exportDataAsked();
    void fileOperationFinished(fileOperation operation);

signals:
    void startNewAcquisition();
    void modelUpdated(void);

    void sendRawData(dataSeries dataToSave);

private:
    explicit dataModel(QWidget *parent = nullptr);
    void updateBounds();
    void fillGraphData();

    dataSeries data;
    QVector<double> minValues;
    QVector<double> maxValues;
    QTime m_lastEmit;
    int m_lastIndexFillGraph;
};

#endif // DATAMODEL_H
