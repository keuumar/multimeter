#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include <QWidget>
#include <QStringList>
#include "qcustomplot/qcustomplot.h"
#include "serialmanager.h"
#include "datamodel.h"

#define POINTS_TO_UPDATE_GRAPH      100

#define CONNECTION_START    "démarrer l'acquisition"
#define CONNECTION_STOP     "arreter l'acquisition"

#define TENSION_COLOR_PEN   QPen(QColor(40, 110, 255))
#define INTENSITY_COLOR_PEN QPen(QColor(55, 128, 20))
#define POWER_COLOR_PEN     QPen(QColor(255, 110, 40))

namespace Ui {
class graphView;
}

class graphView : public QWidget
{
    Q_OBJECT

public:
    explicit graphView(QWidget *parent = nullptr);
    ~graphView();

    void setTheme(QString theme);
    void setAxisConfiguration(QCPAxis * axisToPaint, QString name, QPen penColor = QPen(QColor(0, 0, 0)));
    void initGraphZone();
    void updateGraphZone();

public slots:
    //dialog with controller class
    void connectionStatusUpdate(bool connected);
    void portsListUpdate(QStringList portsName);
    //dialog with model class
    void refreshGraph();

signals:
    //dialog with controller class
    void setComPortName(QString portName);
    void setConnectionState(bool connect);
    void updatePortsList();
    //dialog with model class
    void modelUpdated();
    void askData();
    void giveData();
    //dialog with file export class
    void askToSaveCSV(QString);
    void askToLoadCSV(QString);

private slots:
    void on_startStop_clicked();

    void on_refreshPorts_clicked();

    void on_portCOM_currentIndexChanged(const QString &arg1);

    void on_clearData_clicked();

    void on_loadData_clicked();

    void on_saveData_clicked();

    void on_exportPNG_clicked();

    void on_showU_stateChanged(int arg1);

    void on_showI_stateChanged(int arg1);

    void on_showP_stateChanged(int arg1);

    void on_showFullAcquisition_stateChanged(int arg1);

    void getMouseGraphPosition(QMouseEvent * event);


private:
    Ui::graphView *ui;

    dataModel * model;

    bool m_canRefresh;
    QTimer * m_refreshTimer;

};

#endif // GRAPHVIEW_H
