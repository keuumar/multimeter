#include "exportfilemanager.h"
#include <QTextStream>

exportFileManager::exportFileManager(QWidget *parent) : QWidget(parent)
{

}

void exportFileManager::getSaveFileName(QString saveFileName)
{
    m_saveFileName = saveFileName;
    emit askRawData();
}

void exportFileManager::getLoadFileName(QString loadFileName)
{
    QFile file(loadFileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        emit newRawData(line);
    }
    emit fileOperationFinished(operationLoad);
}

void exportFileManager::GetDataToSave(dataSeries dataToSave)
{
    QFile file(m_saveFileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
       return;

    QTextStream out(&file);
    // entete
    out << "temps (ms)"     << SEPARATOR
        << "tension (V)"    << SEPARATOR
        << "intensité (mA)" << SEPARATOR
        << "puissance (mW)" << NEW_LINE;
    for(int i = 0 ; i < dataToSave.timeSerie.count() ; i++)
    {
        out <<  dataToSave.timeSerie.at(i) * 1000.0 << SEPARATOR
            << dataToSave.tensionSerie.at(i)        << SEPARATOR
            << dataToSave.intensitySerie.at(i)      << SEPARATOR
            << dataToSave.powerSerie.at(i)          << NEW_LINE;
    }

    file.close();
    emit fileOperationFinished(operationSave);
}
