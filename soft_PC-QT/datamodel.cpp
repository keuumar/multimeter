#include "datamodel.h"

#include<QDebug>

dataModel * dataModel::Instance()
{
    static dataModel * instance = NULL;
    if(instance == NULL)
    {
        instance = new dataModel();
    }
    return instance;
}

dataModel::dataModel(QWidget *parent) : QWidget(parent)
{
    clearData();
}

void dataModel::updateBounds()
{
    if(data.tensionSerie.back() > maxValues.at(tension))
        maxValues[tension] = data.tensionSerie.back();
    if(data.intensitySerie.back() > maxValues.at(intensity))
        maxValues[intensity] = data.intensitySerie.back();
    if(data.powerSerie.back() > maxValues.at(power))
        maxValues[power] = data.powerSerie.back();

    if(data.tensionSerie.back() < minValues.at(tension))
        minValues[tension] = data.tensionSerie.back();
    if(data.intensitySerie.back() < minValues.at(intensity))
        minValues[intensity] = data.intensitySerie.back();
    if(data.powerSerie.back() < minValues.at(power))
        minValues[power] = data.powerSerie.back();
}

void dataModel::fillGraphData()
{
    m_lastIndexFillGraph = data.timeSerie.count();
    emit modelUpdated();
    m_lastEmit = QTime::currentTime();
}

void dataModel::processRawData(QByteArray rawData)
{
    QList<QByteArray> values = rawData.split(SEPARATOR);

    if(values.count() < 4)
        return;

    bool isNumeric;
    values[0].toDouble(&isNumeric);
    if(!isNumeric)
        return;

    if(data.timeSerie.count() < 1)
    {
        emit startNewAcquisition();
    }

    int offset = 0;
    data.timeSerie.append(values[0].toDouble()/1000.0);
    offset = 1;

    data.tensionSerie.append(values[tension + offset].toDouble());
    data.intensitySerie.append(values[intensity + offset].toDouble());
    data.powerSerie.append(values[power + offset].toDouble());

    updateBounds();

    if(m_lastEmit.msecsTo(QTime::currentTime()) > MSEC_TO_UPDATE_GRAPH_DATA)
    {
        fillGraphData();
    }
}

void dataModel::clearData()
{
    data.timeSerie.clear();
    data.tensionSerie.clear();
    data.intensitySerie.clear();
    data.powerSerie.clear();
    minValues.clear();
    minValues.append(INIT_MIN_VALUES);
    minValues.append(INIT_MIN_VALUES);
    minValues.append(INIT_MIN_VALUES);
    maxValues.clear();
    maxValues.append(INIT_MAX_VALUES);
    maxValues.append(INIT_MAX_VALUES);
    maxValues.append(INIT_MAX_VALUES);
    emit startNewAcquisition();
    m_lastEmit = QTime::currentTime();
    m_lastIndexFillGraph = 0;
}

void dataModel::exportDataAsked()
{
    emit sendRawData(data);
}

void dataModel::fileOperationFinished(fileOperation operation)
{
    if(operation == operationLoad)
    {
        fillGraphData();
    }
}

dataSeries * dataModel::getAllData()
{
    return &data;
}

dataPoint dataModel::getData(int index)
{
    dataPoint lastValues;
    if(!data.timeSerie.isEmpty())
    {
        if(index >= 0 && index < data.timeSerie.count())
        {
            lastValues.time = data.timeSerie.at(index);
            lastValues.tension = data.tensionSerie.at(index);
            lastValues.intensity = data.intensitySerie.at(index);
            lastValues.power = data.powerSerie.at(index);
        }
        else if(index > -data.timeSerie.count() && index < 0)
        {
            lastValues.time = data.timeSerie.at(data.timeSerie.count() + index);
            lastValues.tension = data.tensionSerie.at(data.timeSerie.count() + index);
            lastValues.intensity = data.intensitySerie.at(data.timeSerie.count() + index);
            lastValues.power = data.powerSerie.at(data.timeSerie.count() + index);
        }
    }
    return lastValues;
}

dataPoint dataModel::findData(double time)
{
    dataPoint values;
    if(!data.timeSerie.isEmpty())
    {
        int index = -1;
        for(int i = 0 ; i < data.timeSerie.count() ; i++)
        {
            if(data.timeSerie.at(i) >= time)
            {
                index = i - 1;
                break;
            }
        }
        if(index >= 0)
            values = getData(index);
    }
    return values;
}

QVector<double> dataModel::getMinValues()
{
    return minValues;
}

QVector<double> dataModel::getMaxValues()
{
    return maxValues;
}
