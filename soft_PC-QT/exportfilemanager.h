#ifndef EXPORTFILEMANAGER_H
#define EXPORTFILEMANAGER_H

#include <QWidget>
#include<QFile>
#include "datamodel.h"

class exportFileManager : public QWidget
{
    Q_OBJECT
public:
    exportFileManager(QWidget *parent = nullptr);

public slots:
    void getSaveFileName(QString saveFileName);
    void getLoadFileName(QString loadFileName);
    void GetDataToSave(dataSeries dataToSave);

signals:
    void askRawData();
    void newRawData(QByteArray);
    void fileOperationFinished(fileOperation);

private:
    QString m_saveFileName;

};

#endif // EXPORTFILEMANAGER_H
