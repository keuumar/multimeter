#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    model = dataModel::Instance();
    view = new graphView();
    controller = new serialManager();
    fileManager = new exportFileManager();

    connect(view        , SIGNAL(setComPortName(QString))               , controller    , SLOT(setComPort(QString)));
    connect(view        , SIGNAL(setConnectionState(bool))              , controller    , SLOT(setConnectionState(bool)));
    connect(view        , SIGNAL(updatePortsList())                     , controller    , SLOT(getPortsInfo()));
    connect(controller  , SIGNAL(connectionStatus(bool))                , view          , SLOT(connectionStatusUpdate(bool)));
    connect(controller  , SIGNAL(portsAvailable(QStringList))           , view          , SLOT(portsListUpdate(QStringList)));
    connect(controller  , SIGNAL(newRawData(QByteArray))                , model         , SLOT(processRawData(QByteArray)));
    connect(model       , SIGNAL(modelUpdated())                        , view          , SLOT(refreshGraph()));
    connect(model       , SIGNAL(startNewAcquisition())                 , view          , SLOT(refreshGraph()));
    connect(fileManager , SIGNAL(askRawData())                          , model         , SLOT(exportDataAsked()));
    connect(fileManager , SIGNAL(newRawData(QByteArray))                , model         , SLOT(processRawData(QByteArray)));
    connect(fileManager , SIGNAL(fileOperationFinished(fileOperation))  , model         , SLOT(fileOperationFinished(fileOperation)));
    connect(model       , SIGNAL(sendRawData(dataSeries))               , fileManager   , SLOT(GetDataToSave(dataSeries)));
    connect(view        , SIGNAL(askToSaveCSV(QString))                 , fileManager   , SLOT(getSaveFileName(QString)));
    connect(view        , SIGNAL(askToLoadCSV(QString))                 , fileManager   , SLOT(getLoadFileName(QString)));
    connect(model       , SIGNAL(startNewAcquisition())                 , controller    , SLOT(resetTimer()));


    ui->gridLayout_3->addWidget(view);
    controller->getPortsInfo();
}

MainWindow::~MainWindow()
{
    delete ui;
}

