#include "graphview.h"
#include "ui_graphview.h"
#include <QDebug>

graphView::graphView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::graphView)
{
    ui->setupUi(this);

    model = dataModel::Instance();
    initGraphZone();
}

graphView::~graphView()
{
    delete ui;
}

void graphView::setTheme(QString theme)
{
    Q_UNUSED(theme)
//    QPalette pal = window()->palette();
//    if (theme == QChart::ChartThemeLight) {
//        pal.setColor(QPalette::Window, QRgb(0xf0f0f0));
//        pal.setColor(QPalette::WindowText, QRgb(0x404044));
//    } else if (theme == QChart::ChartThemeDark) {
//        pal.setColor(QPalette::Window, QRgb(0x121218));
//        pal.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
//    } else if (theme == QChart::ChartThemeBlueCerulean) {
//        pal.setColor(QPalette::Window, QRgb(0x40434a));
//        pal.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
//    } else if (theme == QChart::ChartThemeBrownSand) {
//        pal.setColor(QPalette::Window, QRgb(0x9e8965));
//        pal.setColor(QPalette::WindowText, QRgb(0x404044));
//    } else if (theme == QChart::ChartThemeBlueNcs) {
//        pal.setColor(QPalette::Window, QRgb(0x018bba));
//        pal.setColor(QPalette::WindowText, QRgb(0x404044));
//    } else if (theme == QChart::ChartThemeHighContrast) {
//        pal.setColor(QPalette::Window, QRgb(0xffab03));
//        pal.setColor(QPalette::WindowText, QRgb(0x181818));
//    } else if (theme == QChart::ChartThemeBlueIcy) {
//        pal.setColor(QPalette::Window, QRgb(0xcee7f0));
//        pal.setColor(QPalette::WindowText, QRgb(0x404044));
//    } else {
//        pal.setColor(QPalette::Window, QRgb(0xf0f0f0));
//        pal.setColor(QPalette::WindowText, QRgb(0x404044));
//    }
//    window()->setPalette(pal);
}

void graphView::setAxisConfiguration(QCPAxis * axisToPaint, QString name, QPen penColor)
{
//    axisToPaint->setBasePen(penColor);
//    axisToPaint->setTickPen(penColor);
//    axisToPaint->setSubTickPen(penColor);
    axisToPaint->setTickLabelColor(penColor.color());
    axisToPaint->setLabelColor(penColor.color());
    axisToPaint->setLabel(name);
    axisToPaint->setVisible(true);
}

void graphView::initGraphZone()
{
    setAxisConfiguration(ui->graphZone->xAxis, "temps (s)");
    QCPAxis * intensityAxis = ui->graphZone->axisRect()->addAxis(QCPAxis::atLeft);
    setAxisConfiguration(intensityAxis, "intensité (mA)", INTENSITY_COLOR_PEN);
    setAxisConfiguration(ui->graphZone->yAxis, "tension (V)", TENSION_COLOR_PEN);
    setAxisConfiguration(ui->graphZone->yAxis2, "puissance (mW)", POWER_COLOR_PEN);

    ui->graphZone->xAxis->setRange(0, 60);
    intensityAxis->setRange(0, 500);
    ui->graphZone->yAxis->setRange(0, 7);
    ui->graphZone->yAxis2->setRange(0, 2500);

    ui->graphZone->addGraph();
    ui->graphZone->graph(tension)->setPen(TENSION_COLOR_PEN);
    ui->graphZone->graph(tension)->setName("tension (V)");
    ui->graphZone->addGraph(ui->graphZone->xAxis, intensityAxis);
    ui->graphZone->graph(intensity)->setPen(INTENSITY_COLOR_PEN);
    ui->graphZone->graph(intensity)->setName("intensité (mA)");
    ui->graphZone->addGraph(ui->graphZone->xAxis, ui->graphZone->yAxis2);
    ui->graphZone->graph(power)->setPen(POWER_COLOR_PEN);
    ui->graphZone->graph(power)->setName("puissance (mW)");
    ui->graphZone->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    connect(ui->graphZone, SIGNAL(mouseMove(QMouseEvent *)), this, SLOT(getMouseGraphPosition(QMouseEvent*)));

    ui->graphZone->legend->setVisible(true);
    setTheme("ThemeDark");
    ui->graphZone->repaint();
}

void graphView::updateGraphZone()
{
    ui->graphZone->graph(tension)->setData(model->getAllData()->timeSerie, model->getAllData()->tensionSerie);
    ui->graphZone->graph(intensity)->setData(model->getAllData()->timeSerie, model->getAllData()->intensitySerie);
    ui->graphZone->graph(power)->setData(model->getAllData()->timeSerie, model->getAllData()->powerSerie);

    if(!ui->showU->isChecked()) {
        ui->graphZone->graph(tension)->setVisible(false);
    } else {
        ui->graphZone->graph(tension)->setVisible(true);
    }
    if(!ui->showI->isChecked()) {
        ui->graphZone->graph(intensity)->setVisible(false);
    } else {
        ui->graphZone->graph(intensity)->setVisible(true);
    }
    if(!ui->showP->isChecked()) {
        ui->graphZone->graph(power)->setVisible(false);
    } else {
        ui->graphZone->graph(power)->setVisible(true);
    }

    double minXAxis = 0;
    if(!ui->showFullAcquisition->isChecked())
        minXAxis = model->getData().time - ui->timeStartShow->value();
    ui->graphZone->xAxis->setRange(minXAxis, model->getData().time);
    ui->graphZone->replot();

    if(model->getData().time < 1.0/MSEC_TO_UPDATE_GRAPH_DATA) {
        ui->timeStartShow->setMaximum(1.0/MSEC_TO_UPDATE_GRAPH_DATA);
    } else {
        ui->timeStartShow->setMaximum(model->getData().time);
    }

    if(model->getData().time < 2.0) {
        ui->timeStartShow->setValue(model->getData().time);
    }

}

void graphView::connectionStatusUpdate(bool connected)
{
    if(connected)
    {
        ui->startStop->setText(CONNECTION_STOP);
        ui->portCOM->setEnabled(false);
        ui->refreshPorts->setEnabled(false);
        ui->saveData->setEnabled(false);
        ui->loadData->setEnabled(false);
        ui->exportPNG->setEnabled(false);
        ui->graphZone->setInteractions(0);
    }
    else
    {
        ui->startStop->setText(CONNECTION_START);
        ui->portCOM->setEnabled(true);
        ui->refreshPorts->setEnabled(true);
        ui->saveData->setEnabled(true);
        ui->loadData->setEnabled(true);
        ui->exportPNG->setEnabled(true);
        ui->graphZone->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    }
    ui->startStop->setEnabled(true);
}

void graphView::portsListUpdate(QStringList portsName)
{
    ui->portCOM->clear();
    ui->portCOM->addItems(portsName);
}

void graphView::refreshGraph()
{
    dataPoint lastValues = model->getData();
    ui->instantTime->setValue(lastValues.time);
    ui->instantU->setValue(lastValues.tension);
    ui->instantI->setValue(lastValues.intensity);
    ui->instantP->setValue(lastValues.power);

    updateGraphZone();
}

void graphView::on_startStop_clicked()
{
    ui->startStop->setEnabled(false);
    if(ui->startStop->text() == CONNECTION_START)
    {
        emit setConnectionState(true);
    }
    else
    {
        emit setConnectionState(false);
    }
}

void graphView::on_refreshPorts_clicked()
{
    emit updatePortsList();
}

void graphView::on_portCOM_currentIndexChanged(const QString &arg1)
{
    emit setComPortName(arg1);
}

void graphView::on_clearData_clicked()
{
    model->clearData();
}

void graphView::on_loadData_clicked()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, tr("Ouvrir un fichier de données CSV"), "./", tr("Fichiers CSV (*.csv)"));
    emit askToLoadCSV(fileName);
}

void graphView::on_saveData_clicked()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this, tr("Sauvegarder les données en CSV"), "./", tr("Fichiers CSV (*.csv)"));
    emit askToSaveCSV(fileName);
}

void graphView::on_exportPNG_clicked()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this, tr("Sauvegarder le graphe"), "./", tr("Fichiers PNG (*.png)"));
    ui->graphZone->savePng(fileName);
}

void graphView::on_showU_stateChanged(int arg1)
{
    Q_UNUSED(arg1)
    updateGraphZone();
}

void graphView::on_showI_stateChanged(int arg1)
{
    Q_UNUSED(arg1)
    updateGraphZone();
}

void graphView::on_showP_stateChanged(int arg1)
{
    Q_UNUSED(arg1)
    updateGraphZone();
}

void graphView::on_showFullAcquisition_stateChanged(int arg1)
{
    Q_UNUSED(arg1)
    updateGraphZone();
}

void graphView::getMouseGraphPosition(QMouseEvent *event)
{
    double key;
    double value;
    dataPoint data;
    ui->graphZone->graph(tension)->pixelsToCoords(event->pos(), key, value);
    data = model->findData(key);
    ui->cursorU->setValue(data.tension);
    ui->cursorI->setValue(data.intensity);
    ui->cursorP->setValue(data.power);
    ui->cursorTime->setValue(data.time);
}
