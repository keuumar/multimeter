#include "serialmanager.h"

#include <QMessageBox>
#include <QDebug>
#include <QRandomGenerator>

serialManager::serialManager(QWidget *parent) : QWidget(parent)
{
    m_serial = new QSerialPort();
    m_portName = "";

    connect(m_serial, SIGNAL(readyRead()), this, SLOT(readData()));
    getPortsInfo();
    m_resetTimeStart = true;
    m_debugRandomValues = false;
    if(m_debugRandomValues)
    {
        m_debugTimer = new QTimer();
        m_debugTimer->setInterval(1);
        connect(m_debugTimer, SIGNAL(timeout()), this, SLOT(generateData()));
    }
}

void serialManager::setComPort(QString portName)
{
    m_portName = portName;
}

void serialManager::setConnectionState(bool active)
{
    if(active)
    {
        openSerialPort();
        if(m_debugRandomValues)
            m_debugTimer->start();
    }
    else
    {
        closeSerialPort();
        if(m_debugRandomValues)
            m_debugTimer->stop();
    }
}

void serialManager::getPortsInfo()
{
    const auto infos = QSerialPortInfo::availablePorts();
    QStringList list;
    for (const QSerialPortInfo &info : infos) {
        list.append(info.portName());
    }
    emit portsAvailable(list);
}

void serialManager::openSerialPort()
{
    m_serial->setPortName(m_portName);
    m_serial->setBaudRate(QSerialPort::Baud115200);
    // nb bits, parity, stop and other parameters are left to default values.
    if(!m_debugRandomValues)
    {
        if (!m_serial->open(QIODevice::ReadWrite)) {
            QMessageBox::critical(this, tr("Error"), m_serial->errorString());
        }
        emit connectionStatus(m_serial->isOpen());
    }
    else
    {
        emit connectionStatus(true);
    }
}

void serialManager::closeSerialPort()
{
    if (m_serial->isOpen())
    {
        m_serial->close();
    }
    emit connectionStatus(m_serial->isOpen());
}

void serialManager::writeData(const QByteArray &data)
{
    m_serial->write(data);
}

void serialManager::readData()
{
    if(m_resetTimeStart)
    {
        m_timeStart = QTime::currentTime();
        m_resetTimeStart = false;
    }
    QTime currentTime = QTime::currentTime();
    QByteArray rawRead = m_serial->readAll();
    double offsetTime = 0;
    for (QByteArray line : rawRead.split(NEW_LINE)) {
        if(line.isEmpty())
            continue;
        QString time = QString("%1%2").arg(m_timeStart.msecsTo(currentTime) + offsetTime).arg(SEPARATOR);
        line.insert(0, time);
        emit newRawData(line);
        offsetTime += 0.1;
    }
}

void serialManager::generateData()
{
    QString randomData = QString("%1;%2;%3;%4").arg(m_timeStart.msecsTo(QTime::currentTime())).arg(QRandomGenerator::global()->bounded(5.0)).arg(QRandomGenerator::global()->bounded(500.0)).arg(QRandomGenerator::global()->bounded(2500.0));
    emit newRawData(randomData.toStdString().c_str());
}

void serialManager::resetTimer()
{
    m_resetTimeStart = true;
}
