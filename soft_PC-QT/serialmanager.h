#ifndef SERIALMANAGER_H
#define SERIALMANAGER_H

#include <QWidget>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTime>
#include <QTimer>
#include <QStringList>
#include "datamodel.h"

class serialManager : public QWidget
{
    Q_OBJECT

public:
    explicit serialManager(QWidget *parent = nullptr);

public slots:
    void setComPort(QString portName);
    void setConnectionState(bool active);
    void getPortsInfo();
    void generateData();
    void resetTimer();

signals:
    void newRawData(QByteArray line);
    void connectionStatus(bool connected);
    void portsAvailable(QStringList portsName);

private:
    void openSerialPort();
    void closeSerialPort();

    QSerialPort *m_serial = nullptr;
    QString m_portName;
    QTimer * m_debugTimer;
    bool m_debugRandomValues;
    QTime m_timeStart;
    bool m_resetTimeStart;

private slots:
    void writeData(const QByteArray &data);
    void readData();
};

#endif // SERIALMANAGER_H
